USE LABOR_SQL
GO
--1
select maker, type
from product
where type= 'laptop'
order by maker
--2
select model, ram, screen, price
from laptop
where price >1000
order by ram ASC, price DESC
go
--3
select*
from printer
where color = 'y'
order by price Desc
--4
select model, speed, hd, cd, price
from pc
where cd in ('12x' , '24x') and price<600
order by price DESC
--5  +++
SELECT name, class
FROM [dbo].[ships]
ORDER BY name ASC;
--6
select*
from pc
where speed!<500 and price<800
order by price DESC
--7
select *
from printer
where type != 'Matrix' and price <300
order by type DESC
--8
SELECT model, speed
FROM pc
WHERE price >= 400 AND price <= 600
ORDER BY hd
--9
select model, speed, hd, price
from laptop
where screen !<12
order by price DESC
--10
select model,type, price
from printer
where price <300
order by type DESC
--11
select model, ram, price
from laptop
where ram = 64
order by screen ASC
--12
select model, ram, price
from pc
where ram >64
order by hd 
--13
select model, speed, price
from pc
where speed >=500 and speed <=750
order by hd DESC
--14
select *
from outcome_o
where out >=2000
order by date DESC
--15
select *
from income_o
where inc >=5000 and inc <=10000
order by inc

--16
select *
from income_o
where point = 1
order by inc ASC
--17
select *
from outcome
where point= 2
order by out ASC

--18
select*
from classes
where country = 'Japan'
order by type DESC
--19
select name, launched
from ships
where launched BETWEEN 1920 and 1942
order by launched DESC
--20
select ship, battle, result
from outcomes
where battle = 'Guadalcanal' and result != 'sunk'
order by ship DESC
--21
select ship, battle, result
from outcomes
where result = 'sunk'
order by ship DESC

--22
select class, displacement
from classes
where displacement !<40000
order by type
--23 
select trip_no, town_from, town_to
from trip
where town_from = 'London' or town_to = 'London'
order by time_out ASC
--24
select trip_no, town_from, town_to
from trip
where plane = 'TU-134'
order by time_out DESC
--25 
select trip_no, town_from, town_to
from trip
where plane !='IL-86'
order by plane
--26
select trip_no, town_from, town_to
from trip
where town_from != 'Rostov' and town_to !='Rostov'
order by time_out
--27
select distinct model
from pc
where model like '%11%'
select * from pc
where (select len(model) - len(replace(model,'1',''))) >= 2
go
--28
select *
from outcome
where MONTH (date) = 3
--29 
select * 
from outcome
where DAY(date) = 14
--30
SELECT name
from ships
where name like 'W%n'
--31
select name 
from ships
where LEN(name) - LEN(Replace(name, 'e', ''))=2
--32
select name, launched
from ships
where name not like '%a'
--33
SELECT name
FROM battles
WHERE name LIKE '% %' AND name NOT LIKE '%c'
--34.
SELECT *
FROM [dbo].[trip]
WHERE time_out BETWEEN '12:00:00' AND '17:00:00'

--35.
SELECT *
FROM [dbo].[trip]
WHERE time_in BETWEEN '17:00:00' AND '23:00:00'

--36
SELECT *
FROM trip
WHERE (DATEPART(HOUR, time_in) BETWEEN 21 AND 24) OR (DATEPART(HOUR, time_in) BETWEEN 00 AND 10)
--37
select distinct date
from pass_in_trip
where place like '1%'
--38
select distinct date
from pass_in_trip
where place like '%c'
--39
SELECT ((REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', ''))) as 'Surname'
FROM passenger
WHERE (REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', '')) LIKE 'C%'

--40
SELECT ((REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', ''))) as 'Surname'
FROM passenger
WHERE (REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', '')) NOT LIKE 'J%'
--41 
select '������� ���� =' + CONVERT (Varchar (11), AVG (price))
from Laptop

--42
SELECT CONCAT('���: ', code), CONCAT('������: ', model), CONCAT('��������: ', speed), CONCAT('�ᒺ� �����: ', ram), CONCAT('�����
�����: ', hd), CONCAT('�������� CD-�������: ', cd), CONCAT('����: ', price)
FROM PC
--43 
select convert(varchar, date, 102)
from income
--44
SELECT ship, battle, result =
	CASE result
	WHEN 'OK' THEN '� �������'
	WHEN 'damaged' THEN '�����������'
	WHEN 'sunk' THEN '����������'
	END
FROM outcomes
--45
select trip_no, date, id_psg, '���2'+ LEFT (place,1), '����2'+SUBSTRING(place,2,1)
from pass_in_trip
--46
SELECT trip_no, id_comp, plane,time_out, time_in, CONCAT(' from ', TRIM(town_from), ' to ', town_to) AS 'info'
FROM trip

--47
SELECT CONCAT(LEFT(trip_no, 1), RIGHT(trip_no, 1), ' ', LEFT(id_comp, 1), RIGHT(id_comp, 1), ' ', LEFT(plane, 1), RIGHT(TRIM(plane), 1), ' ', LEFT(town_from, 1), RIGHT(TRIM(town_from), 1), ' ', LEFT(town_to, 1), RIGHT(TRIM(town_to), 1), ' ', LEFT(time_out, 1), RIGHT(time_out, 1), ' ', LEFT(time_in, 1), RIGHT(time_in, 1))
FROM trip

--48
SELECT maker, COUNT(model) AS different_models
FROM [dbo].[product]
WHERE type = 'pc'
GROUP BY maker
HAVING (COUNT(model) > 1)
--49
SELECT town, COUNT (trip_no)
from
(select trip_no, town_from as town FROM trip
union all
select trip_no, town_to as town FROM trip
)a
group by town

--50
select type, COUNT(*)
from printer
group by type
--51
SELECT cd, model, count(DISTINCT cd) as cd_cnt, count(DISTINCT model) as models_cnt
FROM pc
GROUP BY GROUPING SETS(model, cd);

--52
SELECT trip_no, CAST((time_in-time_out) as time(0)) '[hh:mm:ss]'
FROM trip
--53
SELECT point, date, SUM(out), MIN(out), MAX(out)
FROM outcome
GROUP BY point, date WITH ROLLUP

--54
SELECT trip_no, left(place, CHARINDEX(place, place)) AS row, COUNT(left(place, CHARINDEX(place, place))) AS places
FROM [dbo].[pass_in_trip]
GROUP BY trip_no, left(place, CHARINDEX(place, place))
ORDER BY trip_no ASC

--55
SELECT CONCAT('ʳ������: ', COUNT(name)) as Count_name FROM passenger
WHERE name LIKE '% [S,B,A]%' 

