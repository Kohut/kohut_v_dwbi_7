use V_K_Kohut_library
go

INSERT INTO Authors (Author_id, Name, url, birthday)
VALUES
(1, 'J.K. Rrowling', 'www.Rowling.com', '1965-07-31'),
(2, 'Dan Brown', 'www.Brown.com', '1985-02-04'),
(3, 'Kewin Kwan', 'Kwan.com', '1980-08-21'),
(4, 'James Buckley', 'www.Buckley.com', '1981-01-14'); 


insert into Publishers (Publisher_id, Name, url, created, country, City)
values
(21, 'Bloomsbury', 'www.bloomsbury.com', '1986-01-30', 'England', 'London'),
(22, 'Scholastic' , 'www.Scholastic.com', '1920-10-22', 'US', 'NY')
GO


insert into Books (ISBN, Publisher_id, url,price, title, edition, published, issue)
values 
('1245625897', '21', 'www.king.com' , 25, 'Kill all', 35000, '2017-11-11', 1),
('1245625899', '22', 'www.king2.com' , 25, 'Kill all2', 35000, '2017-12-11',2),
('1245625810', '22', 'www.king3.com' , 25, 'Kill all3', 35000, '2018-01-11', 3),
('1245625811', '11', 'www.king4.com' , 25, 'Kill all4', 35000, '2018-03-11', 4)
GO



insert into BooksAuthors (BooksAuthors_id, isbn, Author_id, [Seq_No])
values
(2,'16450211-5035',4,1)  

(4, '16210225-2455  ',3,1)


GO


select*from Authors
go

select*from Publishers

select*from Books

select*from BooksAuthors
go
select *from Authors_Log
GO

