USE V_K_Kohut_library
GO
ALTER TABLE Authors
ADD birthday date
    book_amount int not null check (book_amount >=0 ) DEFAULT 0,
    issue_amount int not null check (issue_amount >=0) DEFAULT 0,
	total_edition int not null check (total_edition >=0) DEFAULT 0
	go

BEGIN TRANSACTION pkdrop

ALTER TABLE BooksAuthors
drop constraint FK_Books_ISBN

declare @pkname VARCHAR(40)
SET @pkname = (SELECT CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY (OBJECT_ID(CONSTRAINT_SCHEMA+ '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey')=1
and TABLE_NAME = 'Books' and TABLE_SCHEMA = 'dbo')
exec ('ALTER TABLE Books DROP CONSTRAINT' + @pkname)

ALTER TABLE Books
add title VARCHAR(60) NOT NULL DEFAULT '''TITLE''',
edition INT NOT NULL CHECK(edition >=1) DEFAULT 1,
published DATE,
issue INT DEFAULT 1,
PRIMARY KEY (ISBN, issue)
;
ALTER TABLE BooksAuthors
ADD CONSTRAINT FK_Books_ISBNs FOREIGN KEY (isbn, SeqNo)
REFERENCES Books(ISBN, isuue)
on update cascade on delete no action

COMMIT TRANSACTION PKdrop
-- confirm
ALTER TABLE Publishers
ADD
created DATE NOT NULL DEFAULT '1900-01-01',
country VARCHAR (30) NOT NULL DEFAULT 'USA',
city VARCHAR (30) NOT NULL DEFAULT 'NY',
book_amount INT NOT NULL CHECK (book_amount >=0) default 0,
issue_amount INT NOT NULL CHECK (issue_amount >=0) default 0,
total_edition INT NOT NULL CHECK (total_edition >=0) default 0
go
--confirmed
ALTER TABLE Authors_log
ADD 
book_amount_old INT,
issue_amount_old INT,
total_edition_old INT,
book_amount_new INT,
issue_amount_new INT,
total_edition_new INT 
go


declare @pkname VARCHAR(40)
SET @pkname = (SELECT TOP (1)  CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY (OBJECT_ID(CONSTRAINT_SCHEMA+ '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey')=1
and TABLE_NAME = 'BooksAuthors' and TABLE_SCHEMA = 'dbo')
exec ('ALTER TABLE BooksAuthors DROP CONSTRAINT' + @pkname)
DECLARE @ukname1 VARCHAR(40)
SET @ukname1 = (SELECT TOP(1) CONSTRAINT_NAME 

FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
where OBJECTPROPERTY(OBJECT_ID (CONSTRAINT_SCHEMA+ '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsUniqueCnst')=1
AND TABLE_NAME='BooksAuthors' AND TABLE_SCHEMA = 'DBO')
 exec ('ALTER TABLE BooksAuthors DROP CONSTRAINT' + @Ukname1)













