use triger_fk_cursor
go

open table_copy
drop table if exists name_table.employee_left
drop table if exists name_table.employee_right

set @table1 = CONCAT(@create, '_left', @sql)
set @table2 = CONCAT(@create, '_right',@sql)
print @table1
execute sp_executesql1 @table1
execute sp_executesql1 @table2
fetch next from table_copy into @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id
while @@FETCH_STATUS=0
begin
set @datetime = getdate()
set @count = ROUND(((rand()*10)+1),0)
if @count <5
begin
insert into name_tabs.employee_left select @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id, @datetime
end
else
begin
insert into name_tabs.employee_right select @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id, @datetime
end
fetch next from table_copy into @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id
end
close table_copy
dellocate table_copy
go

