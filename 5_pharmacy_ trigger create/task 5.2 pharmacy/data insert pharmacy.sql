USE triger_fk_cursor
GO

INSERT INTO zone(name) 
VALUES ('heart'), ('head'), ('stomach'), ('liver'), ('vessels'), ('kinder'), ('eye'), ('nose'),('teeth')
go


INSERT INTO post(post)
VALUES ('Pharmakologist'), ('Distributor'), ('Manager'), ('worker')
GO

INSERT INTO pharmacy(name,building_number, www, streeT)
VALUES 
('one', '125','www. first-farmacy.com','Skrypnuka'),
('2', '2','www. 2-farmacy.com', 'Korynu'),
('3', '32','www. 3-farmacy.com', 'Stryiska'),
('4', '58','www. 4-farmacy.com', 'Kolomyiska'),
('5', '12','www. 5-farmacy.com', 'Vernadskoho'),
('6', '86','www. 6-farmacy.com', 'Chervonoi Kalunu'),
('7', '71','www. 7-farmacy.com', 'Drahana'),
('8', '5','www. 8-farmacy.com', 'Trulyovskogo'),
('9', '8', 'www. 9-farmacy.com', 'Antonycha'),
('10', '2b','www. 10-farmacy.com', 'Skrypnuka')
GO
SET ARITHABORT ON
use triger_fk_cursor

use triger_fk_cursor
go
INSERT INTO employee (surname,[name],midle_name,passport, identity_number,experience,birthday,post,pharmacy_id)
VALUES
('Denisova', 'Olha', 'Valeriyivna', 'KB12548976', '2356412358', 12, '1985', 'MANAGER' ,1),
('Vovk', 'Roman', 'Bohdanovych', 'KA12456885', '1257896123', 15,'1974','pharmacist', 1),
('Bohdanova','Bohdana', 'Bohdanivna','KB25489765', '1253456897', '10','1885','worker',1),
('Ivanov', 'Ivan', 'Tarasovych', 'KB21876458', '2864502368', '12', '1981','manager', 2),
('Sidirova', 'Halyna', 'Fedorivna', 'KA 12486789', '2301547684', '20', ' 1971','pharmacist', 3),
('Karpenko', ' Olena', 'Alexandrivna', 'KB25136578', '1023578942', '13', '03-20-1989', 'manager', 3),
('Savchyn', 'Nadiya', 'Petrivna', 'KB20136547', '0235479812', '30', '03-04-1957', 'worker', 3),
('Dryk', 'Ivanna', 'Vasulivna', 'KB20123654', '0314689715', '11', '01-01-1992', 'pharmacist', 4),
('Stryk','Taras', 'Volodymyrovuch', 'KB10234587', '0134587642', '10', '01-03-1985', 'manager', 5),

GO

INSERT INTO medicine (name, ministry_code)
VALUES
('Analgin', 'OS1103011'),
('Dimedrol', 'ON0011109'),
('Aspirin', 'OA01103'),
('Andypal', 'OT00123'),
('Antygrypin', 'OZ0311199'),
('Amizon', 'PV 0100398'),
('Validol', 'OH0100102')
go

INSERT INTO medicine_zone (medicine_id, zone_id)
VALUES (1,1),(2,3),(3,2),(5,6),(6,7),(2,5),(3,3),(4,8),(5,3),(8,1),(9,3),(5,4)
GO 
INSERT INTO pharmacy_medicine (pharmacy_id,medicine_id)
VALUES 
(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,8),(1,9),
(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),
(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),
(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9)
go


  

