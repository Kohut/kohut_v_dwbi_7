use V_K_Kohut_library
go

CREATE TRIGGER [LibraryIUD] on Authors
AFTER INSERT,UPDATE, DELETE
AS
BEGIN
  IF @@ROWCOUNT = 0 RETURN
  DECLARE @OPERATION CHAR(1)
  DECLARE @INS INT = (select COUNT (*) from inserted)
  DECLARE @DEL INT = (select COUNT (*) from deleted)
   SET @operation =
   CASE
   when @ins >0 AND @del =0 then 'I'
   when @ins >0 AND @del >0 then 'U'
   when @ins = 0 AND @del >0 THEN 'D'
END

IF @operation = 'I'
 BEGIN 
 INSERT INTO Authors_Log
 (Author_id_new, Name_new, [URL_New], Operation_type)
 SELECT inserted.Author_id, inserted.[name], inserted.[URL],
 @OPERATION
 FROM Authors
 INNER JOIN inserted ON Authors.Author_id = inserted.Author_id
 
END

IF @OPERATION = 'U'

BEGIN
UPDATE Authors SET [UPDATED] = GETDATE(), Updated_By = SYSTEM_USER
from Authors
inner join inserted on Authors.Author_id = inserted.Author_id

INSERT INTO Authors_Log
(Author_id_new, Name_new, [URL_New],
Author_id_old, Name_old, [URL_old], Operation_type)
SELECT
inserted.Author_Id, inserted.[Name], inserted.[URL],
deleted.Author_id, deleted.[Name], deleted.[URL],
 
@operation
FROM Authors
INNER JOIN inserted on Authors.Author_Id = inserted.Author_id
INNER JOIN deleted ON Authors.Author_Id = deleted.Author_id
END
IF @OPERATION = 'D'
BEGIN 
INSERT INTO [Authors.Log]

(Author_id_old, [Name.old], [URL.old],
operation_type)

SELECT deleted.Author_id, deleted.[name], deleted.[URL],
@OPERATION
from Authors
 inner join deleted on [Authors].[Author_id]  = deleted.[Author_id]
 END
 END
 GO


CREATE DATABASE [V_K_Kohut_view]
go

 DROP VIEW IF EXISTS [vw_Authors]
 GO
 CREATE OR ALTER VIEW [vw_Authors] (
 [Author_id],
  [Name], 
  [URL], 
  [inserted],
  [inserted_by], 
  [UPDATED],
  [Updated_by]
   )
 AS
 SELECT *FROM V_K_Kohut_library.dbo.Authors
 GO
 -- change table i table create

 Create or alter view [vw_Publishers] (
 Publisher_id,
 [Name],
 [URL],
 [inserted],
 [inserted_by],
 [updated],
 [updated_by]
 )
 AS
 SELECT*from V_K_Kohut_library.dbo.Publishers
  go

 CREATE OR ALTER VIEW [vw_Books](
 [ISBN],
 [Publisher_id],
 [URL],
 [Price],
 [inserted],
 [inserted_by],
 [updated],
 [updated_by]
 )
 AS
 SELECT*FROM V_K_Kohut_library.dbo.vw_Books_Authors
 GO

 CREATE OR ALTER VIEW [vw_Books_Authors]
(
[BooksAuthors_id],
[ISBN],
[Author_id],
[SeqNo],
[inserted],
[inserted_by],
[updated],
[updated_by]
 )
 AS SELECT * FROM V_K_Kohut_library.dbo.Books
 GO

 CREATE OR ALTER VIEW [vw_Authors_log]
 (
 [operation_id],
 [Author_id_new],
 [Name_New],
 [URL_New],
 [Author_id_old],
 [Name_old],
 [URL_old],
 [Operation_type],
 [Operation_datetime]
 )
 AS
 SELECT * FROM V_K_Kohut_library.dbo.Authors_Log
 GO


