create database V_K_Kohut_library
GO

ALTER DATABASE V_K_Kohut_library
ADD filegroup [DATA]
GO
ALTER DATABASE [V_K_Kohut_library]
ADD FILE (Name = 'Library_Main'),
FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\library_Main.mdf'
SIZE=5120kb, FILEGROWTH = 1024kb)
TO FILEGROUP [Data]
GO

use V_K_Kohut_library
go
create table Authors
(Author_id INT NOT NULL PRIMARY KEY,
Name VARCHAR(30) NOT NULL UNIQUE,
[URL] VARCHAR(40) NOT NULL DEFAULT 'www.author_name.com',
inserted DATETIME NOT NULL DEFAULT GETDATE(),
inserted_by VARCHAR (35) not null DEFAULT system_user,
UPDATED DATETIME,
Updated_By VARCHAR(35)
)
GO
CREATE TABLE Publishers
( Publisher_id INT NOT NULL PRIMARY KEY,
Name VARCHAR(20) NOT NULL UNIQUE, 
[URL] VARCHAR (40) NOT NULL  DEFAULT 'www.publisher_name.com',
Inserted DATETIME NOT NULL DEFAULT GETDATE(),
Inserted_By VARCHAR (35) NOT NULL DEFAULT SYSTEM_USER,
Updated DATETIME,
Updated_By VARCHAR(35)
)
GO

CREATE TABLE Books
(
ISBN CHAR (15) NOT NULL PRIMARY KEY,
Publisher_id INT NOT NULL,
[URL] VARCHAR(40) NOT NULL UNIQUE,
Price DECIMAL (6,2) NOT NULL CHECK ( Price >= 0 ) DEFAULT 0,
Inserted DATETIME NOT NULL DEFAULT GETDATE(),
Inserted_By VARCHAR (35) NOT NULL DEFAULT system_user,
Updated DATETIME,
Updated_By VARCHAR (35)
CONSTRAINT FK_Book_Publishers FOREIGN KEY (Publisher_id)
REFERENCES Publishers (Publisher_id)
)
GO

CREATE TABLE BooksAuthors (
BooksAuthors_id INT NOT NULL PRIMARY KEY CHECK (BooksAuthors_id >=1) Default 1,
ISBN CHAR(15) NOT NULL UNIQUE,
Author_id INT NOT NULL UNIQUE,
Seq_No INT NOT NULL CHECK (Seq_No >=1) DEFAULT 1,
Inserted DATETIME NOT NULL DEFAULT GETDATE(),
Inserted_By VARCHAR(35) NOT NULL DEFAULT system_user,
Updated DATETIME,
Updated_By VARCHAR(35)

CONSTRAINT FK_Books_ISBN FOREIGN KEY (ISBN)
REFERENCES Books (ISBN)
ON UPDATE CASCADE  ON DELETE NO ACTION,

CONSTRAINT FK_Books_Authors FOREIGN KEY ( Author_id)
REFERENCES Authors (Author_id)
ON UPDATE NO ACTION ON DELETE NO ACTION

)


CREATE TABLE Authors_Log
(
Operation_id INT NOT NULL IDENTITY PRIMARY KEY,
Author_id_new INT,
Name_new VARCHAR (20),
[URL_New] VARCHAR (40),
Author_id_old INT,
Name_old VARCHAR (20),
[URL_old] VARCHAR (40),
Operation_type CHAR(1) NOT NULL CHECK (Operation_type IN ('I','U','D')),
Operation_datetime DATETIME not null DEFAULT GETDATE())
go
 













