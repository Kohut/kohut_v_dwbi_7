use people_ua_db
go

create table People
(id int primary key identity(0,1) not null,
surname_list_identity_id int,
name_list_identity_id int
)
GO

declare cur cursor FOR 
select id, sex, amount from surname_list_identity
go
use pepole_ua
open cur
declare @surname int
declare @sex nchar
declare @amount int
declare @amount_to_go int
fetch next from cur into @surname, @sex, @amount
while @@FETCH_STATUS=0
begin
set @amount_to_go = @amount
while @amount_to_go>0
begin
if (@sex is null)
insert into People (surname_list_identity_id, name_list_identity_id) values (@surname,(select top 1 id from name_list_identity  order by NEWID()))
 
 else if @sex = 'm'
 insert into People (surname_list_identity_id, name_list_identity_id) values (@surname,(select top 1 id from name_list_identity where sex= 'm' order by NEWID()))
 else if @sex = 'w' 
 insert into  People (surname_list_identity_id, name_list_identity_id) values (@surname,(select top 1 id from name_list_identity where sex = 'w' order by NEWID()))
  set @amount_to_go = @amount_to_go-1

  end
  fetch next from cur into @surname, @sex, @amount
  end
  close cur
  go