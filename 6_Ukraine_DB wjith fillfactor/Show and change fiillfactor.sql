use people_ua_db
go

--show fragmentation for all tables in database and free space
SELECT OBJECT_NAME(ind.OBJECT_ID) AS TableName, 
ind.name AS IndexName, indexstats.index_type_desc AS IndexType, 
indexstats.avg_fragmentation_in_percent, avg_page_space_used_in_percent 
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, 'detailed') indexstats 
INNER JOIN sys.indexes ind  
ON ind.object_id = indexstats.object_id 
AND ind.index_id = indexstats.index_id 
--WHERE indexstats.avg_fragmentation_in_percent > 30 
ORDER BY TableName DESC
go

--creating view
create view Fragmentation as
SELECT OBJECT_NAME(ind.OBJECT_ID) AS TableName, 
ind.name AS IndexName, indexstats.avg_fragmentation_in_percent as Frag_percent
FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, 'detailed') indexstats 
INNER JOIN sys.indexes ind  
ON ind.object_id = indexstats.object_id 
AND ind.index_id = indexstats.index_id 
WHERE indexstats.avg_fragmentation_in_percent > 30 
go

select * from Fragmentation
go

--show fill factor and fragmentation for people
SELECT * FROM sys.dm_db_index_physical_stats  
 (DB_ID('people_ua_db'), OBJECT_ID('People.id'), NULL, NULL , 'detailed') where [object_id] = object_id('People')
 go

 --set fillfactor to 70
 alter index all on testtable rebuild
	with (fillfactor = 70, sort_in_tempdb = on, online = on)
go

--reorganization
ALTER INDEX PK__People_id 
  ON People REORGANIZE;
  go

declare mycur cursor for 
	select TableName, IndexName, Frag_percent from Fragmentation
go

--cursor for automaticly rebuilding or reorganization of index
open mycur
declare @Tname nvarchar(30)
declare @Iname nvarchar(30)
declare @Percent decimal(10,8)
DECLARE @sql nvarchar(500)
fetch next from mycur into @Tname, @Iname, @Percent
while @@FETCH_STATUS = 0
	begin
		if(@Percent >= 50)
			begin
				print 'Rebuilding '+@Iname+' in '+@Tname
				SET @sql = 'alter index '+@Iname+' on '+@Tname+' rebuild with (fillfactor = 70, sort_in_tempdb = on, online = on)'
				EXEC (@sql)
			end
		else if(@Percent >= 30)
			begin
				print 'reorganizing '+@Iname+' in '+@Tname
				SET @sql = 'ALTER INDEX '+@Iname+' ON '+@Tname+' REORGANIZE'
				EXEC (@sql)
			end

		fetch next from mycur into @Tname, @Iname, @Percent
	end
go
close mycur
go

deallocate mycur
go









  
  
  
    
