use labor_sql
go

---1
select ROW_NUMBER() over(order by id_comp, trip_no) as num, trip_no, id_comp
from trip
order by id_comp, trip_no

---2
select ROW_NUMBER() over(Partition by id_comp order by trip_no)as num, trip_no, id_comp
from trip
order by id_comp, trip_no

---3
select model, color, type, price
from 
(select model, color, type, price,
rank () over(partition by type order by price) as price_rank from printer) ss
where price_rank=1

--4
select maker
from (select maker, rank() over(partition by maker order by model) model_rank
from product
where type='pc') ss
where model_rank>2

--5
select distinct price
from (select price, DENSE_RANK() over(order by price desc) as d_rank
from pc) ss
where d_rank=2

--6 ???
select id_psg, name,
       NTILE(3) OVER(ORDER BY sname_l) AS psg_group 
from (select id_psg, name,
			LEN(LTRIM(REVERSE(SUBSTRING(LTRIM(REVERSE(name)), 1, CHARINDEX(' ', LTRIM(REVERSE(name))))))) AS sname_l
		 from passenger
	 ) ss
order by sname_l

--7
select code, model, speed, ram, hd, cd, price,
ROW_NUMBER() over(order by price DESC) as id,
COUNT(*) over() as row_total,
NTILE(cast((select CEILING(count(*)/3.0) from pc) as bigint))over(order by price DESC) as page_num,
(select CEILING(COUNT (*)/3.0) from pc) as page_total
from pc
order by price desc

--8
SELECT max_sum, type, date, point
FROM ( SELECT MAX(inc) OVER() AS max_sum, *
		 FROM (	SELECT inc, 'inc' type, date, point
			FROM income

			UNION ALL

			SELECT inc, 'inc' type, date, point
			FROM income_o

			UNION ALL

			SELECT out, 'out' type, date, point
			FROM outcome

			UNION ALL

			SELECT out, 'out' type, date, point
			FROM outcome_o
		 ) ss2
	 ) ss1
WHERE inc = max_sum

--9

select *, avg(price) over() as total_price
from (select *, price - AVG(price) over () as dif_ottal_price
from (select *, price - AVG(price) over(partition by speed) as dif_total_price
from pc) ss2
)ss1
