use V_K_module3
GO

create trigger sweet_updated_at
ON sweet
AFTER UPDATE 
AS
UPDATE sweet
SET updated_date = getdate() 
WHERE id in (SELECT DISTINCT id FROM inserted) 
GO