use V_K_module3
go 

create table gender
(id int primary key identity (0,1),
name varchar (2),
)
GO

create table cafee_clients
( id int primary key identity (0,1),
cafee_id int not null,
client_id int not null,
inserted_date datetime default getdate(),
updated_date datetime
constraint UniqCafee UNIQUE (cafee_id, client_id)
)
GO

create table cafee_info
(id int primary key identity (0,1),
title varchar (30) not null,
capacity int not null,
cafee_squere int not null,
adress_id int not null,
inserted_date datetime default getdate(),
updated_date datetime
constraint uniqueTitleAdress UNIQUE (title, adress_id)


)
GO

create table sweet_in_cafee
(id int primary key identity (0,1),
sweet_id int not null unique,
cafee_id int not null unique,
inserted_date datetime default getdate(),
updated_date datetime,

)
go

create table sweet
(id int primary key identity (0,1),
title varchar (25) not null,
volume float not null,
price decimal,
provisioner_id int not null,
inserted_date datetime default getdate(),
updated_date datetime
constraint UniqueTitleVolumePriceProvisioner UNIQUE(title, volume, price, provisioner_id)
)
GO

create table adress
(id int primary key identity (0,1),
name varchar(20) not null,
city_id int not null,
inserted_date datetime default getdate(),
updated_date datetime
Constraint UniqueAdressNameCity UNIQUE (name, city_id)
)
GO

create table provisioner
(id int primary key identity (0,1),
title varchar (25) not null,
post_index int not null,
inserted_date datetime default getdate(),
updated_date datetime
Constraint UniqueTitlePostProvisiner UNIQUE (title, post_index)
)
GO

create table city
(id int primary key identity (0,1),
country_id int not null,
city_name varchar (25) not null,
inserted_date datetime default getdate(),
updatetd_date datetime
Constraint UniquNameCountry UNIQUE(city_name, country_id)
)
GO
create table country
(id int primary key identity (0,1),
country_name varchar(30) not null,
country_population int null,
inserted_date datetime default getdate(),
updatetd_date datetime
Constraint UniueTitle UNIQUE (country_name)
)
GO 

create table orders
(id int primary key identity (0,1),
client_id int not null,
cafee_id int not null,
sweet_id int not null,
amount int not null,
inserted_date datetime default getdate(),
updated_date datetime
)
GO
CREATE TABLE avaliable_sweet_in_cafee
(id int not null primary key identity (0,1),
sweet_id int not null,
cafee_id int not null,
inserted_date datetime default getdate(),
updated_date datetime
Constraint UniqSweetCafee UNIQUE (sweet_id, cafee_id)
)
GO







