use V_K_module3
go

alter table cafee_clients
ADD CONSTRAINT FK_cafee_client FOREIGN KEY (client_id) REFERENCES cafee_clients(id)
GO


alter table orders 
ADD CONSTRAINT FK_orders_cleint FOREIGN KEY (client_id) REFERENCES cafee_clients(id)
GO

alter table cafee
add constraint FK_cafeeAdress FOREIGN KEY ( cafee_id) REFERENCES adress(id)
GO


alter table adress
add constraint FK_adressCity FOREIGN KEY (city_id) REFERENCES city(id)
GO
alter table city
add constraint FK_cityCountry FOREIGN KEY (country_id) REFERENCES country(id)
GO
